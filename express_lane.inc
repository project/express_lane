<?php
define('EXPRESS_LANE_CHEKCKOUT_API_VERSION', '93');
function express_lane_set_express_checkout($payment_amount, $currency_code_type, $payment_type, $items, $type, $return_url, $cancel_url) {
  //------------------------------------------------------------------------------------------------------------------------------------
  // Construct the parameter string that describes the SetExpressCheckout API call in the shortcut implementation
  $entity = entity_create('express_lane_checkout', array('status' => 'sendingtopaypal', 'type' => $type, 'total' => $payment_amount, 'created' => time()));

  $nvpstr = "&PAYMENTREQUEST_0_AMT=" . $payment_amount;
  $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_PAYMENTACTION=" . $payment_type;
  $nvpstr = $nvpstr . "&SOLUTIONTYPE=" . 'Sole';
  $nvpstr = $nvpstr . "&NOSHIPPING=" . '1';
  $nvpstr = $nvpstr . "&RETURNURL=" . $return_url;
  $nvpstr = $nvpstr . "&CANCELURL=" . $cancel_url;
  $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_CURRENCYCODE=" . $currency_code_type;
  foreach ($items as $item_id => $item) {
    $position = array_search($item_id, array_keys($items));
    $entity->express_lane_checkout_item[LANGUAGE_NONE][$position] = array('quantity' => intval($item['quantity']), 'eli_id' => $item_id, 'price' => floatval($item['price']));
    $nvpstr = $nvpstr . '&L_PAYMENTREQUEST_0_NAME' . $position . '=' . $item['title'];
    $nvpstr = $nvpstr . '&L_PAYMENTREQUEST_0_NUMBER' . $position . '=' . $item_id;
    $nvpstr = $nvpstr . '&L_PAYMENTREQUEST_0_QTY' . $position . '=' . $item['quantity'];
    $nvpstr = $nvpstr . '&L_PAYMENTREQUEST_0_AMT' . $position . '=' . $item['price'];
  }
  entity_save('express_lane_checkout', $entity);
  $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_PAYMENTREQUESTID=" . $entity->elc_id;
  $_SESSION['express_lane']['details']['checkout_id'] = $entity->elc_id;
  $_SESSION['express_lane']['details']['currencyCodeType'] = $currency_code_type;
  $_SESSION['express_lane']['details']['PaymentType'] = $payment_type;


  //'---------------------------------------------------------------------------------------------------------------
  //' Make the API call to PayPal
  //' If the API call succeded, then redirect the buyer to PayPal to begin to authorize payment.
  //' If an error occured, show the resulting errors
  //'---------------------------------------------------------------------------------------------------------------
  if ($payment_amount < 10000) {
    $res_array = express_lane_hash_call("SetExpressCheckout", $nvpstr);
    $ack = strtoupper($res_array["ACK"]);
    if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
      $entity->status = 'paypalconnectionestablished';
      $token = urldecode($res_array["TOKEN"]);
      $_SESSION['express_lane']['details']['TOKEN'] = $token;
      express_lane_redirect_to_paypal($token);
    }
    else {
      $entity->status = 'paypalconnectionfailed';
      watchdog('express_lane', t('express_lane_set_express_checkout returned %ack', array('%ack' => $ack)), NULL, WATCHDOG_ERROR);

      drupal_set_message(t('Whoops! This is rather embarassing, we were\'t able to establish a secure connection to PayPal. Please hang tight and try again. Have no fear, you won\'t be charged for our mistake. If the problem persists, please let us know via <a href="@emailaddress" target="_blank">%email</a> and we will find a work around', array('%email' => variable_get('site_mail'), '@emailaddress' => 'mailto:' . variable_get('express_lane_email'))), 'error');
    }
    entity_save('express_lane_checkout', $entity);
    return $res_array;
  }
  else {
    $entity->status = 'contactforoverpplimit';
    entity_save('express_lane_checkout', $entity);
    $id = 'checkout-' . $entity->elc_id;
    $hash = rtrim(strtr(base64_encode($id), '+/', '-_'), '=');
    $_SESSION['express_lane']['over-limit']['checkout-' . $entity->elc_id] = $entity;
    drupal_goto('express-gratitude/' . $hash);
  }
}

function express_lane_get_details($token) {
  //'--------------------------------------------------------------
  //' At this point, the buyer has completed authorizing the payment
  //' at PayPal.  The function will call PayPal to obtain the details
  //' of the authorization, incuding any shipping information of the
  //' buyer.  Remember, the authorization is not a completed transaction
  //' at this state - the buyer still needs an additional step to finalize
  //' the transaction
  //'--------------------------------------------------------------

  //'---------------------------------------------------------------------------
  //' Build a second API request to PayPal, using the token as the
  //'  ID to get the details on the payment authorization
  //'---------------------------------------------------------------------------
  $nvpstr = "&TOKEN=" . $token;

  //'---------------------------------------------------------------------------
  //' Make the API call and store the results in an array.
  //'	If the call was a success, show the authorization details, and provide
  //' 	an action to complete the payment.
  //'	If failed, show the error
  //'---------------------------------------------------------------------------
  $res_array = express_lane_hash_call("GetExpressCheckoutDetails", $nvpstr);
  $ack = strtoupper($res_array["ACK"]);
  if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
    $entity = express_lane_checkout_load($res_array['PAYMENTREQUEST_0_PAYMENTREQUESTID']);
    $entity->email = $res_array['EMAIL'];
    $entity->pp_payer_id = $res_array['PAYERID'];
    $entity->country_code = $res_array['COUNTRYCODE'];
    $entity->total = $res_array['AMT'];
    $entity->tax = $res_array['TAXAMT'];
    $entity->first_name = $res_array['FIRSTNAME'];
    $entity->last_name = $res_array['LASTNAME'];
    $entity->status = $res_array['CHECKOUTSTATUS'];
    entity_save('express_lane_checkout', $entity);
    $_SESSION['express_lane']['details']['payer_id'] = $res_array['PAYERID'];
  }
  elseif ($ack == "FAILURE" || $ack == "FAILUREWITHWARNING") {
    if (isset($res_array['PAYMENTREQUEST_0_PAYMENTREQUESTID'])) {
      $entity = express_lane_checkout_load($res_array['PAYMENTREQUEST_0_PAYMENTREQUESTID']);
      $entity->status = $res_array['CHECKOUTSTATUS'];
      if (isset($res_array['L_LONGMESSAGE0'])) {
        $entity->pending_reason = $res_array['L_LONGMESSAGE0'];
      }
      entity_save('express_lane_checkout', $entity);
    }
    watchdog('express_lane', t('express_lane_get_details returned %ack', array('%ack' => $ack)), NULL, WATCHDOG_ERROR);
  }
  else {
    watchdog('express_lane', t('express_lane_get_details could not establish paypal connection.'), NULL, WATCHDOG_ERROR);
  }
  return $res_array;
}

function express_lane_confirm_payment($final_payment_amt, $type = 'cart', $details) {
  /* Gather the information to make the final call to
		   finalize the PayPal payment.  The variable nvpstr
		   holds the name value pairs
		   */






  //Format the other parameters that were stored in the session from the previous calls
  $token = urlencode($_SESSION['express_lane']['details']['TOKEN']);
  $payment_type = urlencode($_SESSION['express_lane']['details']['PaymentType']);
  $currency_code_type = urlencode($_SESSION['express_lane']['details']['currencyCodeType']);
  $payer_id = urlencode($_SESSION['express_lane']['details']['payer_id']);

  $server_name = urlencode($_SERVER['SERVER_NAME']);
  $nvpstr = '&TOKEN=' . $token . '&PAYERID=' . $payer_id . '&PAYMENTREQUEST_0_PAYMENTACTION=' . $payment_type . '&PAYMENTREQUEST_0_AMT=' . $final_payment_amt;
  $nvpstr .= '&PAYMENTREQUEST_0_CURRENCYCODE=' . $currency_code_type . '&IPADDRESS=' . $server_name . '&PAYMENTREQUEST_0_INVNUM=' . $details['PAYMENTREQUEST_0_PAYMENTREQUESTID'];

  /* Make the call to PayPal to finalize payment
		    If an error occured, show the resulting errors
		    */




  $res_array = express_lane_hash_call("DoExpressCheckoutPayment", $nvpstr);

  /* Display the API response back to the browser.
		   If the response from PayPal was a success, display the response parameters'
		   If the response was an error, display the errors received using APIError.php.
		   */



  $entity = express_lane_checkout_load($details['PAYMENTREQUEST_0_PAYMENTREQUESTID']);

  $ack = strtoupper($res_array["ACK"]);
  if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
    $entity->status = $res_array['PAYMENTINFO_0_PAYMENTSTATUS'];
    $entity->pending_reason = $res_array['PAYMENTINFO_0_PENDINGREASON'];
    $status = strtoupper($res_array['PAYMENTINFO_0_PAYMENTSTATUS']);
    $entity->total = $res_array['PAYMENTINFO_0_AMT'];
    $entity->tax = $res_array['PAYMENTINFO_0_TAXAMT'];
    $entity->fee = $res_array['PAYMENTINFO_0_FEEAMT'];
    $entity->transaction_id = $res_array['PAYMENTINFO_0_TRANSACTIONID'];
    if ($type == 'cart' && ($status == 'COMPLETED' || $status == 'PROCESSED' || $status == 'COMPLETED-FUNDS-HELD')) {
      if (isset($_SESSION['express_lane']['over-limit'])) {
        unset($_SESSION['express_lane']['details']);
        unset($_SESSION['express_lane']['cart']);
      }
      else {
        unset($_SESSION['express_lane']);
      }
    }
    elseif ($type == 'direct' && ($status == 'COMPLETED' || $status == 'PROCESSED' || $status == 'COMPLETED-FUNDS-HELD')) {
      unset($_SESSION['express_lane']['details']);
    }
  }
  elseif ($ack == "FAILURE" || $ack == "FAILUREWITHWARNING") {
    watchdog('express_lane', t('express_lane_confirm_payment returned %ack for checkout id: %elcid', array('%ack' => $ack, '%elcid' => $entity->elc_id)), NULL, WATCHDOG_ERROR);
    $entity->status = 'failureonfpaymentcomfirmation';
    if (isset($res_array['L_LONGMESSAGE0'])) {
      $entity->pending_reason = $res_array['L_LONGMESSAGE0'];
    }
  }
  else {
    $entity->status = 'paymentconfirmationconnectionnotestablished';
    watchdog('express_lane', t('express_lane_confirm_payment could not establish paypal connection for checkout id: %elcid', array('%elcid' => $entity->elc_id)), NULL, WATCHDOG_ERROR);
  }
  $entity->completed = time();
  entity_save('express_lane_checkout', $entity);
  module_invoke_all('express_lane_checkout', $entity);
  return $res_array;
}

function express_lane_hash_call($method_name, $nvp_str) {
  //declaring of global variables


  $user = variable_get('express_lane_user');
  $pwd = variable_get('express_lane_password');
  $signature = variable_get('express_lane_signature');
  $useraction = variable_get('express_lane_environment');
  //setting the curl parameters.
  $ch = curl_init();
  if (variable_get('express_lane_environment') == 'sandbox') {
    $api_endpoint = "https://api-3t.sandbox.paypal.com/nvp";
  }
  else {
    $api_endpoint = "https://api-3t.paypal.com/nvp";
  }
  curl_setopt($ch, CURLOPT_URL, $api_endpoint);
  curl_setopt($ch, CURLOPT_VERBOSE, 1);

  //turning off the server and peer verification(TrustManager Concept).
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, 1);


  //NVPRequest for submitting to server
  $nvpreq = "METHOD=" . urlencode($method_name) . "&VERSION=" . urlencode(EXPRESS_LANE_CHEKCKOUT_API_VERSION) . "&PWD=" . urlencode($pwd) . "&USER=" . urlencode($user) . "&SIGNATURE=" . urlencode($signature) . $nvp_str;

  //setting the nvpreq as POST FIELD to curl
  curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

  //getting response from server
  $response = curl_exec($ch);

  //convrting NVPResponse to an Associative Array
  $nvp_res_array = express_lane_deformat_nvp($response);
  $nvp_req_array = express_lane_deformat_nvp($nvpreq);

  if (curl_errno($ch)) {
    // moving to display page to display curl errors
    $_SESSION['express_lane']['details']['curl_error_no'] = curl_errno($ch);
    $_SESSION['express_lane']['details']['curl_error_msg'] = curl_error($ch);

    //Execute the Error handling module to display errors.
  }
  else {
    //closing the curl
    curl_close($ch);
  }

  return $nvp_res_array;
}

function express_lane_redirect_to_paypal($token) {
  //Set PayPal URL here.
  if (variable_get('express_lane_environment') == 'sandbox') {
    $paypal_url = 'https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=' . $token;
  }
  else {
    $paypal_url = 'https://www.paypal.com/webscr?cmd=_express-checkout&token=' . $token;
  }
  if (variable_get('express_lane_useraction') == 'commit') {
    $paypal_url .= '&useraction=commit';
  }
  // Redirect to paypal.com here.
  header("Location: " . $paypal_url);
  exit;
}

function express_lane_deformat_nvp($nvpstr) {
  $intial = 0;
  $nvp_array = array();

  while (strlen($nvpstr)) {
    //postion of Key
    $keypos = strpos($nvpstr, '=');
    //position of value
    $valuepos = strpos($nvpstr, '&') ? strpos($nvpstr, '&') : strlen($nvpstr);

    /*getting the Key and Value values and storing in a Associative Array*/




    $keyval = substr($nvpstr, $intial, $keypos);
    $valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);
    //decoding the respose
    $nvp_array[urldecode($keyval)] = urldecode($valval);
    $nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
  }
  return $nvp_array;
}

