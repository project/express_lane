
--- Project Description ---
Express Lane is a streamlined platform to integrate PayPal Express Checkout. It's
intended to be easy to configure, but powerful enough to use as an e-commerce 
or e-fundraising platform that can grow as the organizations using it grow.

While this project is intended to be lightweight, it does provide some neat features:
*Gift Emails
*Display of PayPal fee vs Total Payment
*Easy styling through custom CSS
*Direct Buy and Cart purchasing
*Sandbox and Live Environments
*Choice of User Confirmation or Direct PayPal Processing
*Easy to customize language for Cart and Buy buttons.

That said, there are several features that still need work. Among those are:
*Shipping - there is currently storage for product shipping rates, but not for customer's
shipping info. Ideally it will simply send the shipping fee each shippable transaction to
PayPal and then process shipping information on PayPal along with payment info. 
*Refunds - there is no mechanism to account for this yet, so it needs to be handled 
through PayPal.
*Custom path for shop page.
*Rules integration
*Views integration
*Multiple currency - currently only USD support.

Express Lane does not store customer information other than customer name, email and 
transaction ID. It does provide space to fill out more details, such as Phone # if a
transaction exceeds PayPals $10,000 USD limit.

With these things in mind, Express Lane is currently best suited for a simple fundraising
platform such as that seen on https://theor.org/shop for which the project was created by
j.branson D-UID 2136706.

Co-maintainers are welcome to help develop this project further. Feel free to get in touch
about working together via branson@theor.us.

--- Email Configuration ---
It's important to have the PHP email system verified on your server so that automatically
emailed messages do not get marked as spam. 


