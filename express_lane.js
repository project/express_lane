/**
 * @file
 * Javascript behaviors for the express_lane module node form.
 */

(function ($) {

Drupal.behaviors.express_laneFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.express-lane-options', context).drupalSetSummary(function (context) {
      var $input = $('.form-item-express-lane-item-active input');
      var val = $input.val();

      if ($('.form-item-express-lane-item-active input').attr('checked')) {
        return Drupal.t('Active Item');
      }
      else{
        return Drupal.t('Not an Item');
      }
    });
  }
};

})(jQuery);